# Les missions

Nous allons maintenant développer le module mission.

## TP : Module mission

1. Ajouter un module `MissionModule` sur le même principe que le module `AgentModule`
2. Déplacer le component `MissionListComponent` dans le module `MissionModule`
3. Ajouter un component `MissionListTableComponent` pour afficher le tableau des missions
4. Ajouter deux colonnes supplémentaires contenant respectivement un bouton `Modifier` et un bouton `Supprimer`
5. Au-dessus du tableau, ajouter un bouton `Ajouter une mission`

## TP : Le service mission

Voici un exemple d'un objet Mission : 

```ts
{
    cost: 680,
    country: "Australie",
    date_from: "2021-01-13",
    date_to: "2021-02-10",
    id_mission: 1,
    ref_agent: 2
}
```

 1. Dans le module `MissionModule` ajouter une interface `mission.model` pour définir l'objet `Mission`
 2. Dans le service `MissionService` ajouter la méthode `loadMissionList` pour charger les missions
 3. Dans la méthode `loadMissionList` appeler l'URL `http://localhost:3000/missions` en `GET` pour charger les missions
 3. Dans le service `MissionService` ajouter la méthode `addNewMission` pour ajouter une nouvelle mission (laisser vide pour le moment)
 3. Dans le service `MissionService` ajouter la méthode `editMission` pour éditer une mission (laisser vide pour le moment)
 4. Dans le service `MissionService` ajouter la méthode `deleteMission` pour supprimer une mission (laisser vide pour le moment)

**Résultat attendu :**

![mission_s1](img/mission_s1.png#center)

## TP : Prénom et nom de l'agent

Dans l'affichage des missions on peut voir la référence vers l'ID de l'agent concerné par la mission. Il faudrait pouvoir afficher le prénom et le nom de l'agent pour plus de clarté.

 1. Dans le module `MissionListComponent` charger le service `AgentService`
 2. Dans la fonction d'initialisation du component, appeler le chargement des agents
 3. Ajouter la liste des agents en input dans le component `MissionListTableComponent`
 4. Dans le component `MissionListTableComponent`, ajouter une méthode `getFullNameByIdAgent()` qui prend en paramètre l'ID d'un agent et qui retourne le nom complet (prénom + nom)

**Résultat attendu :**

![mission_s2](img/mission_s2.png#center)

## TP : Supprimer une mission 

 1. Dans le component `MissionListTableComponent` ajouter un paramètre de sortie `deleteMission`
 2. Un clic sur le bouton de suppression doit appeler le paramètre de sortie `deleteMission` avec l'ID de la mission
 3. Dans `MissionListComponent`, à l'appel du paramètre `deleteMission`, exécuter la méthode `deleteMission` du service `MissionService`
 4. Appeler l'URL `http://localhost:3000/missions/del/:idMission` avec la méthode `DELETE` pour supprimer une mission
 5. Au retour de la requête envoyer un `toastr` à l'utilisateur pour l'informer de la suppression de la mission et supprimer la mission du tableau `missions`

**Résultat attendu :**

![mission_s3](img/mission_s3.png#center)

## TP : Ajouter une nouvelle mission

Dans cette partie nous allons créer un formulaire réactif pour créer une nouvelle mission.

 1. Importer le module `ReactiveFormsModule` depuis `@angular/forms` dans le module `mission`
 2. Créer un nouveau component `MissionFormComponent`
 3. Dans le component `MissionFormComponent`, importer le service `FormBuilder`
 4. Créer le formulaire réactif `missionForm` qui doit correspondre à l'objet mission (`cost`, `country`, `date_from`, `date_to`, `ref_agent`)
 5. Dans la vue du component `MissionFormComponent` créer le formulaire HTML et lier votre formulaire `missionForm`
 6. Dans le component `MissionFormComponent`, ajouter une variable en input `agents` de type `Agent[]`
 7. Dans le component `MissionFormComponent`, ajouter une variable en output `onSubmit` de type `EventEmitter<Mission>`
 8. Dans le component `MissionFormComponent`, ajouter une méthode `submit` 
 9. Dans la vue du component `MissionFormComponent`, lier l'évènement `ngSubmit` avec la méthode `submit`
 10. La méthode `submit` doit envoyer un nouvel objet mission dans la variable d'output `onSubmit`
 11. Dans la vue du component `MissionFormComponent` le champ de formulaire Agent doit être un `select` avec la liste des agents. Pour cela utiliser la propriété `agents`.
 12. Importer le module `BsDatepickerModule.forRoot()` depuis `ngx-bootstrap/datepicker` dans le module `mission`
 13. Dans la vue du component `MissionFormComponent` les champs de formulaire `date_from` et `date_to` doivent ouvrir un `datepicker` de sélection. Pour cela ajouter l'attribut `bsDatepicker` et `[bsConfig]="{ dateInputFormat: 'YYYY-MM-DD', isAnimated: true }"` sur les deux inputs
 14. Dans la méthode `submit` du component `MissionFormComponent` il faut appeler `toISOString().split('T')[0]` sur les valeurs de `date_from` et `date_to` pour avoir les valeurs au format 'YYYY-MM-DD'
 15. Créer un nouveau component `NewMissionComponent` et importer les services `MissionService` et `AgentService`
 16. Dans la vue du component `NewMissionComponent` appeler le component `MissionFormComponent`. L'évenement de sortie `onSubmit` doit appeler la méthode `addNewMission` du service `MissionService`
 16. Dans le service `MissionService` la méthode `addNewMission(mission: Mission)` doit appeler l'URL `http://localhost:3000/missions/add` en POST. Au retour de la réponse, il faut envoyer un `toastr` pour prévenir que la mission a bien été créée et renvoyer vers la page `mission-list`
 17. Dans le routeur `MissionRoutingModule`, ajouter une route `new-mission` vers le component `NewMissionComponent`
 18. Dans le component `MissionListComponent` régler le `routerLink` du bouton ajouter une mission vers la route `/new-mission` du backend

**Résultat attendu :**

![mission_s4](img/mission_s4.png#center)

![mission_s5](img/mission_s5.png#center)

## TP : Éditer une mission

Dans cette partie nous allons voir comment ré-utiliser le formulaire de mission pour éditer une mission existante.

Il est possible de passer une variable dans une route Angular. Pour cela il faut définir une route avec un paramètre comme ceci :

```ts
const routes: Routes = [
    { path: 'edit-book/:id_book', component: EditBookComponent },
];
```

Ici nous avons définis une variable `id_book` et si l'utilisateur appel `/edit-book/1` alors `id_book` vaudra 1.

Pour récupérer la variable dans le component, il faut importer le service `ActivatedRoute` et appeler `snapshot.paramMap.get('id_book')` pour avoir la valeur, exemple : 

```ts
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router'

@Component({
    selector: 'app-edit-book',
    templateUrl: './edit-book.component.html',
    styleUrls: ['./edit-book.component.scss']
})
export class EditBookComponent {
    constructor(private route: ActivatedRoute) { }

    getIdBook(): number {
        return this.route.snapshot.paramMap.get('id_book');
    }
}
```

 1. Dans le component `MissionFormComponent`, ajouter une variable en input `mission` de type `Mission`
 2. Ajouter un nouveau component `EditMissionComponent` et importer les services `MissionService` et `AgentService`
 3. Ajouter une nouvelle route `edit-mission/:id_mission` vers le component `EditMissionComponent`
 4. Dans la vue `EditMissionComponent` l'évenement de sortie `onSubmit` doit appeler la méthode `editMission` du service `MissionService`
 5. La méthode `editMission` du service `MissionService` doit appeler l'URL `http://localhost:3000/missions/update/:id_mission` en PUT. Au retour de la réponse, il faut envoyer un `toastr` pour informer que la mission a bien été éditée puis renvoyer l'utilisateur vers la page `mission-list`
 6. Dans la vue du component `EditMissionComponent` appeler le component `MissionFormComponent` et envoyer la mission qui correspond à `id_mission` passé en paramètre de la route.
 7. Dans le component `MissionFormComponent` implémenter l'interface `OnInit` et ajouter la méthode `ngOnInit`
 8. Si la variable d'input `mission` est différent de `null` alors éditer le formulaire avec les valeurs de `mission`.
 9. Dans la méthode `submit` si la variable d'input `mission` est `null` alors mettre `id_mission = 0` sinon `id_mission` doit être égale à `mission.id_mission`

**Résultat attendu :**

![mission_s6](img/mission_s6.png#center)

![mission_s7](img/mission_s7.png#center)