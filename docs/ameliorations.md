# Les améliorations

Vous pouvez améliorer cette application avec des fonctionnalités supplémentaires : 

 - Éviter le rechargement de `missions` ou `agents` lorsque l'utilisateur navigue de page en page
 - Permettre d'ajouter, de modifier ou de supprimer un agent
 - Ajouter un module `status` pour gérer les status des agents
 - Ajouter un formulaire de recherche pour le tableau des missions (Recherche par date ou pays par exemple)
 - Ajouter la possibilité de voir toutes les missions d'un agent à partir du tableau des agents