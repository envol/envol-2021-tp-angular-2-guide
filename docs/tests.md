# Les tests unitaires

Nous allons voir dans cette partie comment écrire un test unitaire et comment l'exécuter.

## Jest

Nous avons choisi ici d'exécuter nos tests unitaires avec le framework JEST. Pour lancer les tests il suffira d'appeler la commande make suivante : 

```
make tests_client
```

## Architecture d'un fichier de test

Pour tester un `component` il faut ajouter un fichier qui porte le même nom que le component mais avec l'extension `.spec.ts`.
Par exemple pour tester le component `AppComponent` il faut ajouter un fichier `app.component.spec.ts`.

Le fichier test du component doit contenir une fonction `describe` et cette fonction prend deux paramètres : 

 - Un commentaire de description du fichier testé qui sera affiché à l'éxecution
 - La fonction de `callback` exécutée

Exemple : 

```ts
describe('AppComponent', () => {
    // Les tests doivent être ajoutés ici
});
```

La fonction spéciale `beforeEach` sera appelée avant l'exécution de chaque test. 

Cette fonction va nous permettre, par exemple, de configurer l'environement d'exécution comme l'import des dépendances, le mock des services...

Exemple : 

```ts
describe('AppComponent', () => {
    beforeEach(() => {
        // Configuration
    });
});
```

Enfin pour ajouter un test il faut faire appel à la fonction `it` qui prend deux paramètres de manière similaire à la fonction `describe` :

 - Un commentaire de description du test qui sera affiché lors de l'exécution
 - La fonction de `callback` exécutée

Exemple : 

```ts
describe('AppComponent', () => {
    beforeEach(() => {
        // Configuration
    });

    it('should create app component', () => {
        // Test la fonctionnalité
    });
});
```

## Angular testing

Angular met à la disposition du développeur un package pour faciliter les tests unitaires dans `@angular/core/testing`.
Ici nous allons utiliser `TestBed` pour configurer notre environnement d'exécution et créer le component à tester.
Le component à tester est stocké dans une variable accessible par tous les tests.

Exemple :

```ts
import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
    let app: AppComponent;

    beforeEach(() => {
        // Configuration de l'environement d'exécution des tests
        TestBed.configureTestingModule({
            declarations: [
                AppComponent
            ],
        }).compileComponents();
        const fixture = TestBed.createComponent(AppComponent);
        app = fixture.componentInstance;
    });

    it('should create the app', () => {
        expect(app).toBeTruthy();
    });
});
```

Pour tester une valeur il faut utiliser la fonction `expect` qui prend comme paramètre la valeur à tester et appeler une méthode de validation.
Ici `toBeTruthy` s'assure que le component `AppComponent` est bien créé.

## TP : Tester le component MissionListTableComponent

 1. Ajouter le fichier `mission-list-table.component.spec.ts`
 2. Appeler la fonction `describe` avec comme commentaire `Test du component MissionListTableComponent`
 3. Ajouter une variable `component` de type `MissionListTableComponent` qui prendra l'instance du component à tester
 4. Appeler la fonction `beforeEach()` pour configurer l'environement d'exécution
 5. Dans `configureTestingModule` ajouter la clé `imports` qui prend comme valeur un tableau
 6. Dans le tableau des imports ajouter `RouterTestingModule` depuis `@angular/router/testing`
 7. Ajouter un test avec la fonction `it` pour s'assurer que le component est bien créé
 8. Ajouter un test avec la fonction `it` pour tester la fonction `getFullNameByIdAgent`
 9. Ajouter un agent au tableau d'agents du component à tester
 10. Tester le retour de la fonction `getFullNameByIdAgent()` avec la méthode `toEqual`
 11. Lancer les tests avec `make tests_client`