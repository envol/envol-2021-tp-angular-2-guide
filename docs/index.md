# TP Angular n°2

Nous allons mettre en œuvre dans ce TP :

 - L'installation de l'environnement de développement pour le TP Angular n°2
 - Le développement d'une interface graphique pour l'application backend express développée précédemment

## Déroulement du TP

Le TP comprend un `docker-compose` qui permettra d'installer :

- un serveur de base de données
- le serveur backend express
- une interface web pour visualiser la base de données missions
- l'application frontend

## Guide

À chaque étape du TP, vous serez guidés pour la mise en oeuvre de votre application avec des indications et vous pourrez visualiser le résultat attendu sous forme de screenshot.

## Auteurs

Ce guide a été réalisé par : 

 - `François Agneray` : Laboratoire d'Astrophysique de Marseille
 - `Tifenn Guillas` : Laboratoire d'Astrophysique de Marseille
 