# Les agents

Maintenant que nous avons géré l'authentification nous pouvons développer notre application.
Nous allons commencer par le module `agent` qui va nous permettre de charger la liste des agents présents dans la base de données.
En effet, le module `mission` nécessite d'avoir la liste des agents pour fonctionner.

## TP : Module agent

 1. Ajouter un nouveau module `AgentModule` avec un fichier de routing `AgentRoutingModule` et un service `AgentService`
 2. Créer un component `AgentListComponent` comportant un titre `Liste des agents`
 3. Ajouter une route `agent-list` dans `AgentRoutingModule` qui pointe sur le component `AgentListComponent`. N'oubliez pas de le protéger avec le `Guard`.
 4. Importer `AgentModule` dans le module principal `AppModule`
 5. Ajouter une barre de menu dans le `AppComponent` avant le `<router-outlet></router-outlet>` pour naviguer entre les pages `mission-list` et `agent-list`
 6. À droite dans la barre de menu, ajouter un bouton de déconnexion qui fait appel à la méthode `clearTokenFromLocalStorage` de `LoginService`
 7. Le bouton de déconnexion doit effacer le `userToken` et renvoyer l'utilisateur sur la page `login`
 8. La barre de menu doit être visible uniquement lorsque l'utilisateur est connecté

## TP: Service agent

Voici un exemple d'un objet agent : 

```ts
{
    id_agent: 6,
    firstname: "Aliev",
    lastname: "Kallistrat",
    status: "Chercheur·se",
    cap: 1500
}
```

 1. Dans le module `AgentModule` ajouter une interface `agent.model` pour définir l'objet `Agent`
 2. Dans le service `AgentService` ajouter une propriété `agents` qui stock la liste des agents et deux propriétés `agentsIsLoading`, `agentsIsLoaded`
 3. Ajouter une méthode `loadAgentList` qui permettra de charger la liste des agents et d'affecter le résultat à la propriété `agents`
 4. Dans le component `AgentListComponent`, appeler lors de l'initialisation la méthode `loadAgentList` du service `AgentService`
 5. Dans le module `AgentModule` ajouter un component `AgentListTableComponent`
 6. Passer en paramètre d'entrée de `AgentListTableComponent` la liste `agents`
 7. À l'aide d'une table bootstrap afficher les agents dans `AgentListTableComponent`
 8. La table ne doit être visible que si `agentsIsLoaded` est vrai
 9. Si `agentsIsLoading` est vrai, afficher un spinner

**Résultat attendu :**

![agent_s1](img/agent_s1.png#center)