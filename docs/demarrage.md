# Démarrage du TP

Dans cette partie nous allons :

 - Cloner le dépôt permettant de commencer le TP
 - Installer les dépendances (backend + frontend)
 - Démarrer l'application

## Récupération du dépôt

Pour pouvoir démarrer le TP, nous allons cloner le dépôt git contenant les fichiers sources de base : [https://gitlab.lam.fr/envol/envol-2021-tp-angular-2.git](https://gitlab.lam.fr/envol/envol-2021-tp-angular-2.git)

```sh
git clone https://gitlab.lam.fr/envol/envol-2021-tp-angular-2.git
```

À chaque étape du TP vous pourrez retrouver une branche correspondante. Dans la branche master vous pouvez retrouver l'application finalisée.

Par défaut le dépôt est positionné sur la branche `demarrage`.

Pour changer de branche vous pouvez utiliser la commande `checkout` suivi du nom de la branche, exemple : 

```sh
git checkout demarrage
```

## Installation des dépendances

Pour fonctionner, le projet a besoin de dépendances au niveau du backend et du frontend. Pour cela nous avons prévu deux raccourcis :

```sh
make install_server
make install_client
```

Les dépendances sont téléchargées et installées dans le dossier `server/node_modules` et `client/node_modules`.

## Démarrage de l'application

Nous pouvons maintenant démarrer l'application. La commande `make start` permet de faciliter cette action pour lancer les serveurs `backend`, `frontend` et de base de données.

```sh
make start
```

- Vous pouvez accéder à la version de développement du backend à l'adresse : `http://localhost:3000`
- Vous pouvez accéder à la version de développement du frontend à l'adresse : `http://localhost:4200`
- Vous pouvez également accéder à une interface graphique pour vous connecter à la base de données sur : `http://localhost:8080`